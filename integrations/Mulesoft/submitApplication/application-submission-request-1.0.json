﻿{
    "$schema": "http://json-schema.org/draft-04/schema#",
    "type": "object",
    "title": "SubmitApplication",
    "description": "This is the schema that defines the requirements for Annuity Application",
    "additionalProperties": false,
    "properties": {
        "SubmitApplication": {
            "type": "object",
            "additionalProperties": true,
            "properties": {
                "correlationId": {
                    "type": "string"
                },
                "policyId": {
                    "type": "string"
                },
                "policyProductCode": {
                    "type": "string"
                },
                "primaryAnnuitant": {
                    "$ref": "#/definitions/User"
                },
                "jointAnnuitant": {
                    "$ref": "#/definitions/User"
                },
                "joint": {
                    "type": "boolean"
                },
                "initialPaymentAccount": {
                    "$ref": "#/definitions/PaymentAccount"
                },
                "recurringPaymentAccount": {
                    "$ref": "#/definitions/PaymentAccount"
                },
                "initialContributionAmount": {
                    "type": "number"
                },
                "recurringContributionAmount": {
                    "type": "number"
                },
                "primaryBeneficiaries": {
                    "type": "array",
                    "items": {
                        "$ref": "#/definitions/Beneficiary"
                    }
                },
                "secondaryBeneficiaries": {
                    "type": "array",
                    "items": {
                        "$ref": "#/definitions/Beneficiary"
                    }
                },
                "documents": {
                    "type": "array",
                    "items": {
                        "$ref": "#/definitions/Document"
                    }
                },
                "paymentSchedule": {
                    "$ref": "#/definitions/PaymentSchedule"
                },
                "autoPay": {
                    "type": "boolean"
                },
                "quote": {
                    "$ref": "#/definitions/Quote"
                },
                "withHolding": {
                    "$ref": "#/definitions/Withholding"
                },
                "producer": {
                    "$ref": "#/definitions/Producer"
                },
                "suitability": {
                    "$ref": "#/definitions/Suitability"
                }
            },
            "required": [
                "policyId",
                "primaryAnnuitant",
                "joint",
                "initialPaymentAccount",
                "initialContributionAmount",
                "primaryBeneficiaries",
                "autoPay",
                "quote",
                "correlationId",
                "withHolding",
                "producer",
                "documents"
            ]
        }
    },
    "definitions": {
        "User": {
            "type": "object",
            "additionalProperties": true,
            "properties": {
                "uid": {
                    "type": "string"
                },
                "firstName": {
                    "type": "string"
                },
                "middleName": {
                    "type": "string"
                },
                "lastName": {
                    "type": "string"
                },
                "email": {
                    "type": "string"
                },
                "gender": {
                    "$ref": "#/definitions/Gender"
                },
                "birthDate": {
                    "type": "string",
                    "pattern": "^(\\d{4})-(\\d{2})-(\\d{2})$"
                },
                "phoneNumber": {
                    "$ref": "#/definitions/PhoneNumber"
                },
                "address": {
                    "$ref": "#/definitions/Address"
                },
                "ssn": {
                    "$ref": "#/definitions/SSN"
                },
                "residencyStatus": {
                    "type": "string",
                    "enum": [
                        "us_citizen",
                        "permanent_resident"
                    ]
                }
            },
            "required": [
                "uid",
                "firstName",
                "middleName",
                "lastName",
                "email",
                "gender",
                "birthDate",
                "phoneNumber",
                "address",
                "ssn",
                "residencyStatus"
            ]
        },
        "Gender": {
            "type": "string",
            "enum": [
                "M",
                "F"
            ]
        },
        "Address": {
            "type": "object",
            "additionalProperties": true,
            "properties": {
                "Address": {
                    "type": "object",
                    "properties": {
                        "uid": {
                            "type": "string"
                        },
                        "addressLineOne": {
                            "type": "string"
                        },
                        "addressLineTwo": {
                            "type": "string"
                        },
                        "city": {
                            "type": "string"
                        },
                        "state": {
                            "type": "string"
                        },
                        "zipcode": {
                            "type": "string"
                        }
                    },
                    "required": [
                        "uid",
                        "addressLineOne",
                        "city",
                        "state",
                        "zipcode"
                    ]
                }
            }
        },
        "Beneficiary": {
            "type": "object",
            "additionalProperties": true,
            "properties": {
                "uid": {
                    "type": "string"
                },
                "firstName": {
                    "type": "string"
                },
                "lastName": {
                    "type": "string"
                },
                "relationship": {
                    "type": "string",
                    "enum": [
                        "spouse",
                        "other"
                    ]
                },
                "addressLineOne": {
                    "type": "string"
                },
                "addressLineTwo": {
                    "type": "string"
                },
                "city": {
                    "type": "string"
                },
                "state": {
                    "type": "string"
                },
                "zipcode": {
                    "type": "string"
                },
                "phoneNumber": {
                    "$ref": "#/definitions/PhoneNumber"
                },
                "ssn": {
                    "$ref": "#/definitions/SSN"
                },
                "allocationPercent": {
                    "type": "number",
                    "example": 0.5
                },
                "birthDate": {
                    "type": "string",
                    "pattern": "^(\\d{4})-(\\d{2})-(\\d{2})$"
                },
                "email": {
                    "type": "string"
                }
            },
            "required": [
                "uid",
                "firstName",
                "lastName",
                "relationship",
                "addressLineOne",
                "addressLineTwo",
                "city",
                "state",
                "zipcode",
                "phoneNumber",
                "ssn",
                "allocationPercent",
                "birthDate"
            ]
        },
        "ContributionFrequency": {
            "type": "string"
        },
        "PhoneNumber": {
            "type": "string",
            "pattern": "^\\(\\d{3}\\) \\d{3}-\\d{4}$"
        },
        "SSN": {
            "type": "string",
            "pattern": "^\\d{3}-\\d{2}-\\d{4}$"
        },
        "FundType": {
            "type": "string",
            "enum": [
                "nonqualified",
                "traditional_ira",
                "roth_ira"
            ]
        },
        "PaymentAccountType": {
            "type": "string",
            "enum": [
                "checking",
                "savings",
                "traditional_ira",
                "roth_ira"
            ]
        },
        "PaymentAccount": {
            "type": "object",
            "additionalProperties": true,
            "properties": {
                "uid": {
                    "type": "string"
                },
                "type": {
                    "$ref": "#/definitions/PaymentAccountType"
                },
                "accountNumber": {
                    "type": "string"
                },
                "accountAbaRoutingNumber": {
                    "type": "string"
                },
                "accountWireRoutingNumber": {
                    "type": "string"
                }
            },
            "required": [
                "uid",
                "type"
            ]
        },
        "PaymentSchedule": {
            "type": "object",
            "additionalProperties": true,
            "properties": {
                "uid": {
                    "type": "string"
                },
                "annualIncreasePercent": {
                    "type": "number"
                },
                "contributionFrequency": {
                    "$ref": "#/definitions/ContributionFrequency"
                },
                "paymentDayOfMonth": {
                    "type": "integer"
                },
                "paymentMonthOfYear": {
                    "type": "integer"
                },
                "startDate": {
                    "type": "string",
                    "pattern": "^(\\d{4})-(\\d{2})-(\\d{2})$"
                },
                "endDate": {
                    "type": "string",
                    "pattern": "^(\\d{4})-(\\d{2})-(\\d{2})$"
                }
            },
            "required": [
                "uid",
                "contributionFrequency",
                "startDate",
                "endDate"
            ]
        },
        "Quote": {
            "type": "object",
            "additionalProperties": true,
            "properties": {
                "uid": {
                    "type": "string"
                },
                "premiumAmount": {
                    "type": "number"
                },
                "incomeAmount": {
                    "type": "number"
                },
                "excludedIncomeAmount": {
                    "type": "number"
                },
                "paymentFrequency": {
                    "$ref": "#/definitions/ContributionFrequency"
                },
                "birthDate": {
                    "type": "string",
                    "pattern": "^(\\d{4})-(\\d{2})-(\\d{2})$"
                },
                "incomeStartDate": {
                    "type": "string",
                    "pattern": "^(\\d{4})-(\\d{2})-(\\d{2})$"
                },
                "gender": {
                    "$ref": "#/definitions/Gender"
                },
                "quoteType": {
                    "$ref": "#/definitions/QuoteType"
                },
                "state": {
                    "$ref": "#/definitions/State"
                },
                "fundType": {
                    "$ref": "#/definitions/FundType"
                },
                "deathBenefit": {
                    "type": "boolean"
                },
                "returnOfPremium": {
                    "type": "boolean"
                },
                "joint": {
                    "type": "boolean"
                },
                "jointBirthDate": {
                    "type": "string",
                    "pattern": "^(\\d{4})-(\\d{2})-(\\d{2})$"
                },
                "jointGender": {
                    "type": "string",
                    "enum": [
                        "M",
                        "F"
                    ]
                },
                "primaryContinuation": {
                    "type": "integer"
                },
                "jointContinuation": {
                    "type": "integer"
                },
                "certainPeriod": {
                    "type": "integer"
                },
                "createdAt": {
                    "type": "string",
                    "pattern": "^(\\d{4})-(\\d{2})-(\\d{2})T(\\d{2})(:)(\\d{2})(:)(\\d{2})(\\.\\d+)?(Z|([+-])(\\d{2})(:)?(\\d{2}))$"
                }
            },
            "required": [
                "uid",
                "premiumAmount",
                "incomeAmount",
                "excludedIncomeAmount",
                "paymentFrequency",
                "birthDate",
                "incomeStartDate",
                "gender",
                "quoteType",
                "state",
                "fundType",
                "deathBenefit",
                "returnOfPremium",
                "joint",
                "createdAt"
            ]
        },
        "Withholding": {
            "type": "object",
            "properties": {
                "federal": {
                    "type": "number"
                },
                "federalPct": {
                    "type": "number"
                },
                "federalType": {
                    "$ref": "#/definitions/WithHoldingType"
                },
                "federalAmount": {
                    "type": "number"
                },
                "state": {
                    "type": "number"
                },
                "stateType": {
                    "$ref": "#/definitions/WithHoldingType"
                },
                "statePct": {
                    "type": "number"
                },
                "stateAmount": {
                    "type": "number"
                },
                "allowances": {
                    "type": "number"
                },
                "additionalAmount": {
                    "type": "number"
                },
                "maritalStatus": {
                    "$ref": "#/definitions/MaritalStatus"
                }
            },
            "required": [
                "allowances",
                "additionalAmount",
                "maritalStatus"
            ]
        },
        "MaritalStatus": {
            "type": "string",
            "enum": [
                "single",
                "married"
            ]
        },
        "WithHoldingType": {
            "type": "string",
            "enum": [
                "amount",
                "percent"
            ]
        },
        "QuoteType": {
            "type": "string",
            "enum": [
                "premium",
                "income"
            ]
        },
        "State": {
            "type": "string"
        },
        "Producer": {
            "type": "object",
            "properties": {
                "producerCertification": {
                    "type": "boolean"
                },
                "producerCertificationExplanation": {
                    "type": "string"
                },
                "producerFirstName": {
                    "type": "string"
                },
                "producerLastName": {
                    "type": "string"
                },
                "producerMiddleName": {
                    "type": "string"
                },
                "producerEmail": {
                    "type": "string"
                },
                "producerCommissionPct": {
                    "type": "number"
                },
                "producerCommissionOption": {
                    "$ref": "#/definitions/ProducerCommisionOption"
                },
                "insurerProducerNumber": {
                    "type": "string"
                },
                "producerInsuranceLicenseNumber": {
                    "type": "string"
                },
                "brokerDealerName": {
                    "type": "string"
                },
                "agencyName": {
                    "type": "string"
                },
                "agencyId": {
                    "type": "string"
                },
                "agencyAddress": {
                    "$ref": "#/definitions/Address"
                },
                "agencyPhoneNumber": {
                    "$ref": "#/definitions/PhoneNumber"
                }
            },
            "required": [
                "producerCertification",
                "producerCertificationExplanation",
                "producerFirstName",
                "producerLastName",
                "producerMiddleName",
                "producerEmail",
                "producerCommissionPct",
                "producerCommissionOption",
                "insurerProducerNumber",
                "producerInsuranceLicenseNumber",
                "brokerDealerName",
                "agencyName",
                "agencyId",
                "agencyAddress",
                "agencyPhoneNumber"
            ]
        },
        "ProducerCommisionOption": {
            "type": "string",
            "enum": [
                "lump_sum",
                "trail"
            ]
        },
        "Document": {
            "type": "object",
            "additionalProperties": true,
            "properties": {
                "documentId": {
                    "type": "string"
                },
                "name": {
                    "type": "string"
                },
                "type": {
                    "$ref": "#/definitions/DocumentType"
                },
                "description": {
                    "type": "string"
                },
                "contentType": {
                    "$ref": "#/definitions/DocumentContentType"
                },
                "contentLength": {
                    "type": "integer"
                },
                "createdAt": {
                    "type": "string"
                }
            },
            "required": [
                "documentId",
                "type",
                "contentType",
                "contentLength",
                "createdAt"
            ]
        },
        "DocumentType": {
            "type": "string",
            "enum": [
                "application",
                "contract"
            ]
        },
        "DocumentContentType": {
            "type": "string",
            "enum": [
                "application/pdf"
            ]
        },
        "Suitability": {
            "type": "object",
            "properties": {
                "isReplacement": {
                    "type": "boolean"
                },
                "hasExistingInsurance": {
                    "type": "boolean"
                }
            },
            "required": [
                "isReplacement",
                "hasExistingInsurance"
            ]
        }
    }
}